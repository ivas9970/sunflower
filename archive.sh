#!/bin/bash

P1=(cellar number flag exchange meeting fairies cobweb harmony mass quiet base use store tramp zinc care holiday expansion team spiders wound bath ground creator bell tub jellyfish rain hole actor trip teaching whip tent sheet bulb transport bite match anger boundary flavor hook root iron prose vest jar day cave rule rest growth rhythm oatmeal motion watch plane ticket afterthought unit top soda request monkey coach hall cart wax swim station potato hose verse change wall thing curve mask queen scene branch history camera writing ball join earth library butter stomach parcel pig feeling visitor truck night produce push lamp rub bridge edge language pail wine ring pizzas camp sun surprise water dinosaurs position girl island health adjustment pin control girls account zipper talk loss kiss plastic current donkey turn giants tax pencil worm boat thread fang sock instrument cake agreement apparatus fish cactus story head brake toes wave son mark connection cakes hands slip structure birth border representative juice trade badge bear honey grandmother desk grain window mouth pest pipe rate grandfather soup corn partner downtown country respect territory note book eyes lip coal church toy insect crib rabbit ladybug thumb kittens shelf selection writer laugh chalk order basket sack swing hand baby experience hydrant porter deer direction coil nation cap suit winter soap friends comparison nose earthquake seashore show elbow spring range purpose curtain squirrel cemetery wilderness rose burst kick tomatoes steel cannon hospital road system calendar trains horse ship lunch ghost friend frogs afternoon gate stocking sponge hate houses stage cheese debt amount event party fold engine bushes sand mitten sort linen knot snail fireman egg orange trouble sea quilt credit design discussion trail smell income force business religion hair face rifle knowledge skirt bubble club tongue quince cough gun quicksand play milk pets cast process calculate analyze prepare)
P2=(plastic drain salt waves cent scarf gate train ladybug throat addition cub desire taste lettuce dogs competition ice suggestion quiet card blood carpenter lace smile representative substance camp brother connection aunt horse spoon degree minute toad mind digestion shame skin existence toothbrush division sugar suit hydrant furniture cemetery magic window yak snake event railway cherry arch stomach loss horn ink jar leg creator lock quarter wilderness beef sister notebook crow cow pleasure unit snakes jewel approval kick cloth snow hate side fairies work volleyball back hand root chicken sleep stamp sound line egg rings toothpaste flight bedroom ocean mark crime teeth transport soda oranges spade quince payment coach birth motion animal calendar doctor cover pest trousers stretch dinosaurs spiders star crayon women partner seashore donkey profit lake afternoon rake toe top feeling yarn watch lamp daughter distance powder cars woman ring run pet calculator plantation wealth key wrench harpoon solace)
P3=(jelly distribution bead orange sticks shake passenger friction science mountain zipper holiday board cherries ladybug bit growth home card reason touch afternoon bat cakes hydrant adjustment tray nation form system bed view hat fall flame design leather hate leg shape calculator cook guide giants playground calendar silver memory breath silk kitty letters pancake wilderness offer governor tree mother tail history drawer control example mouth crowd thing invention current pets soda books space writing field face turn cabbage party cake money territory scissors elbow butter van yard tomatoes exchange sense thought airplane destruction fork digestion advice ants creator rainstorm river quill sky curve side pen collar shop match sweater whip crate industry cherry bedroom tongue show laugh steam boat week ring anger vegetable carriage ball whistle coat jewel position edge women trail quiver cover furniture existence use zinc celery recess blow library pigs pet dinner hall swing class toy geese letter)

file_count="${1:-5}"
input_file="${2:-sunflower.ipynb}"
output_dir="${3:-upload-to-google-drive}"

rm -rf "${output_dir}" && mkdir -p "${output_dir}"

OS=`uname`
for (( i=1; i<=file_count; i++ ))
do
  if (( RANDOM % 2 )); then name2_separator="_"; elif (( RANDOM % 2 )); then name2_separator="-"; else name2_separator=""; fi
  if (( RANDOM % 2 )); then name2="$name2_separator${P2[RANDOM%150]}"; else name2=""; fi
  if (( RANDOM % 2 )); then name3_separator="_"; elif (( RANDOM % 2 )); then name3_separator="-"; else name3_separator=""; fi
  if (( RANDOM % 2 )); then name3="$name3_separator${P3[RANDOM%150]}"; else name3=""; fi
  if (( RANDOM % 2 )); then name4_separator="_"; elif (( RANDOM % 2 )); then name4_separator="-"; else name4_separator=""; fi
  if (( RANDOM % 2 )); then name4="$name4_separator${P1[RANDOM%302]}"; else name4=""; fi

  id="${P1[RANDOM%302]}${name2}${name3}${name4}"
  output_file="${output_dir}/${id}.ipynb"

  cp "${input_file}" "${output_file}"

  if [ "$OS" = 'Darwin' ]; then
      sed -i "" "s/_replace_me_with_unique_name_in_latin_/${id}/g" "${output_file}"
  else
      sed -i'' -e "s/_replace_me_with_unique_name_in_latin_/${id}/g" "${output_file}"
  fi

done

rm "unzip-and-${output_dir}.zip" 2> /dev/null || true
zip -r "unzip-and-${output_dir}.zip" "${output_dir}"
rm -rf "${output_dir}"
